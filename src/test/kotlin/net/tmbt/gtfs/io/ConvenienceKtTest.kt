package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.*
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.exists
import org.jetbrains.exposed.sql.transactions.transaction
import org.testng.Assert.*
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import kotlin.io.path.createTempFile
import kotlin.reflect.KProperty

/**
 * Unit tests for the convenience features in io.Convenience.kt
 */
internal class ConvenienceKtTest {

    lateinit var dataSetPath: File

    @BeforeClass
    fun initDatabase() {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver", user = "root", password = "")
        dataSetPath = createTempFile("testdata", ".zip").toFile()
        dataSetPath.deleteOnExit()

        javaClass.classLoader.getResourceAsStream("testdata.zip")!!.use {
            Files.copy(it, dataSetPath.toPath(), StandardCopyOption.REPLACE_EXISTING)
        }
    }

    @Test
    fun testDatabaseSetup() {
        transaction {
            updateDatabase()

            listOf(
                AgencyTable,
                LevelTable,
                StopTable,
                RouteTable,
                CalendarTable,
                CalendarDateTable,
                ShapePointTable,
                TripTable,
                StopTimeTable,
                FareAttributeTable,
                FareRuleTable,
                FrequencyTable,
                TransferTable,
                PathwayTable,
                FeedInfoTable,
                TranslationTable,
                AttributionTable
            ).forEach { table ->
                assertTrue(table.exists())
            }
        }
    }

    @Test(dependsOnMethods = ["testDatabaseSetup"])
    fun testImportGtfsDatasetPath() {
        transaction {
            updateDatabase()
            importGtfsDataset(dataSetPath.toPath())
            testDataSet()
        }
    }

    @Test(dependsOnMethods = ["testDatabaseSetup"])
    fun testImportGtfsDatasetUrl() {
        transaction {
            updateDatabase()
            importGtfsDataset(dataSetPath.toPath().toUri().toURL(), inMemory = true)
            testDataSet()
        }
    }

    /**
     * A function to test the integrity of the imported data set. Does not test for the correctness of imported values,
     * but merely for their presence. This means they still could be wrong (e.g. values interchanged within columns) but
     * at least no columns are missing, that are present in the dataset
     */
    private fun testDataSet() {
        val agencies = Agency.all()
        assertEquals(agencies.count(), 1)
        assertOptionalsPresent(agencies, Agency::lang, Agency::phone)

        assertEquals(Calendar.all().count(), 1)
        assertEquals(CalendarDate.all().count(), 1)

        val routes = Route.all()
        assertEquals(routes.count(), 1)
        assertOptionalsPresent(routes, Route::agency, Route::shortName, Route::color, Route::textColor)

        val stopTimes = StopTime.all()
        assertEquals(stopTimes.count(), 1)
        assertOptionalsPresent(
            stopTimes,
            StopTime::arrivalTime,
            StopTime::departureTime,
            StopTime::pickupType,
            StopTime::dropOffType
        )

        val stops = Stop.all()
        assertEquals(stops.count(), 4)
        assertOptionalsPresent(
            stops,
            Stop::name,
            Stop::lat,
            Stop::lon
        )

        val trips = Trip.all()
        assertEquals(trips.count(), 1)
        assertOptionalsPresent(
            trips,
            Trip::headsign,
            Trip::direction,
            Trip::bikesAllowed
        )
    }

    private fun assertOptionalsPresent(entities: Iterable<Entity<*>>, vararg properties: KProperty<*>) {
        entities.forEach { entity ->
            properties.forEach { property ->
                assertNotNull(
                    property.getter.call(entity),
                    "${property.name} is not present in ${entity.javaClass.simpleName} \"${entity.id}\""
                )
            }
        }
    }
}