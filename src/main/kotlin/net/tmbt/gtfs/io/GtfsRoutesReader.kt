package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.AgencyTable
import net.tmbt.gtfs.model.PickupMode
import net.tmbt.gtfs.model.RouteTable
import net.tmbt.gtfs.model.RouteType
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsRoutesReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        RouteTable.batchInsert(entries, ignore = false) { row ->
            val entityId = EntityID(row["route_id"] ?: error("cannot create route without route_id"), RouteTable)

            this[RouteTable.id] = entityId
            this[RouteTable.agency] = row["agency_id"]?.let { EntityID(it, AgencyTable) }
            this[RouteTable.shortName] = row["route_short_name"]
            this[RouteTable.longName] = row["route_long_name"]
            this[RouteTable.description] = row["route_desc"]
            this[RouteTable.type] = RouteType.byOrdinalOrNull(row["route_type"]?.toInt())
                ?: error("cannot create route without type")
            this[RouteTable.url] = row["route_url"]
            this[RouteTable.color] = row["route_color"]
            this[RouteTable.textColor] = row["route_text_color"]
            this[RouteTable.sortOrder] = row["route_sort_order"]?.toInt()
            this[RouteTable.continuousPickup] = PickupMode.byOrdinalOrNull(row["continuous_pickup"]?.toInt())
            this[RouteTable.continuousDropOff] = PickupMode.byOrdinalOrNull(row["continuous_drop_off"]?.toInt())
        }
    }
}