package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.AgencyTable
import net.tmbt.gtfs.model.AttributionTable
import net.tmbt.gtfs.model.RouteTable
import net.tmbt.gtfs.model.TripTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsAttributionReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        AttributionTable.batchInsert(entries, ignore = false) { row ->
            this[AttributionTable.attributionId] = row["attribution_id"]
            this[AttributionTable.agencyId] = row["agency_id"]?.let { EntityID(it, AgencyTable) }
            this[AttributionTable.routeId] = row["route_id"]?.let { EntityID(it, RouteTable) }
            this[AttributionTable.tripId] = row["trip_id"]?.let { EntityID(it, TripTable) }
            this[AttributionTable.organizationName] = row["organization_name"]
                ?: error("cannot create an attribution without organization_name")
            this[AttributionTable.isProducer] = row["is_producer"]?.let { it == "1" }
            this[AttributionTable.isOperator] = row["is_operator"]?.let { it == "1" }
            this[AttributionTable.isAuthority] = row["is_authority"]?.let { it == "1" }
            this[AttributionTable.attributionUrl] = row["attribution_url"]
            this[AttributionTable.attributionEmail] = row["attribution_email"]
            this[AttributionTable.attributionPhone] = row["attribution_phone"]
        }
    }
}