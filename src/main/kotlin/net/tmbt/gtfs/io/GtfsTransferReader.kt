package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.StopTable
import net.tmbt.gtfs.model.TransferTable
import net.tmbt.gtfs.model.TransferType
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsTransferReader(inputStream: InputStream) : GtfsReader(inputStream) {

    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        TransferTable.batchInsert(entries, ignore = false) { row ->
            this[TransferTable.fromStopId] =
                EntityID(row["from_stop_id"] ?: error("cannot create transfer without from_stop_id"), StopTable)
            this[TransferTable.toStopId] =
                EntityID(row["to_stop_id"] ?: error("cannot create transfer without to_stop_id"), StopTable)
            this[TransferTable.transferType] = TransferType.byOrdinalOrNull(row["transfer_type"]?.toInt())
                ?: error("cannot create transfer without transfer type")
            this[TransferTable.minTransferTime] = row["transfer_time"]?.toInt()
        }
    }
}