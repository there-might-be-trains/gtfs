package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.*
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsStopTimeReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        StopTimeTable.batchInsert(entries, ignore = false) { row ->
            this[StopTimeTable.trip] = row["trip_id"]?.let { EntityID(it, TripTable) }
                ?: error("cannot create stop time without trip_id")
            this[StopTimeTable.stop] = row["stop_id"]?.let { EntityID(it, StopTable) }
                ?: error("cannot create stop time without stop_id")
            this[StopTimeTable.arrivalTime] = row["arrival_time"]
            this[StopTimeTable.departureTime] = row["departure_time"]
            this[StopTimeTable.stopSequence] =
                row["stop_sequence"]?.toInt() ?: error("cannot create stop time without stop_sequence")
            this[StopTimeTable.headSign] = row["stop_headsign"]
            this[StopTimeTable.pickupType] = PickupMode.byOrdinalOrNull(row["pickup_type"]?.toInt())
            this[StopTimeTable.dropOffType] = PickupMode.byOrdinalOrNull(row["drop_off_type"]?.toInt())
            this[StopTimeTable.continuousPickup] = PickupMode.byOrdinalOrNull(row["continuous_pickup"]?.toInt())
            this[StopTimeTable.continuous] = PickupMode.byOrdinalOrNull(row["continuous_drop_off"]?.toInt())
            this[StopTimeTable.shapeDist] = row["shape_dist_traveled"]?.toFloat()
            this[StopTimeTable.timePoint] = TimeMode.byOrdinalOrNull(row["timepoint"]?.toInt())
        }
    }
}