package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.FareAttributeTable
import net.tmbt.gtfs.model.FareRuleTable
import net.tmbt.gtfs.model.RouteTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsFareRuleReader(inputStream: InputStream) : GtfsReader(inputStream) {

    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        FareRuleTable.batchInsert(entries, ignore = false) { row ->
            this[FareRuleTable.fareId] =
                EntityID(row["fare_id"] ?: error("cannot create fare rule without fare id"), FareAttributeTable)

            this[FareRuleTable.route] = row["route_id"]?.let { EntityID(it, RouteTable) }
            this[FareRuleTable.origin] = row["origin"]
            this[FareRuleTable.destination] = row["destination"]
            this[FareRuleTable.contains] = row["contains"]
        }
    }
}