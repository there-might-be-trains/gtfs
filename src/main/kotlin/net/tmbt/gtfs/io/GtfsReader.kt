package net.tmbt.gtfs.io

import net.tmbt.gtfs.util.UnicodeBOMInputStream
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.Closeable
import java.io.InputStream

private const val DEFAULT_CHUNK_SIZE = 1000

/**
 * Read a GTFS structure from an [InputStream].
 * The specific sub-classes all read one type of file into the thread-local database.
 * See [the GTFS reference](https://developers.google.com/transit/gtfs/reference#dataset_files) for details.
 *
 * @param inputStream provides a character stream conforming to GTFS
 *
 * @param ID the data type of the identifier for this specific GTFS record. Usually [String].
 */
abstract class GtfsReader(private val inputStream: InputStream) : Closeable {
    private val parser =
        CSVParser(UnicodeBOMInputStream(inputStream).skipBOM().reader(), CSVFormat.RFC4180.withHeader())
    private val recordIterator = parser.iterator()
    private val gtfsHeader: List<String>

    private val chunkSize by lazy {
        val property = try {
            System.getProperty("gtfs.reader.chunksize")
        } catch (e: SecurityException) {
            LogManager.getLogger()
                .warn("reading property gtfs.reader.chunksize failed because access is restricted by a SecurityManager")
            null
        }

        val value = if (property != null) {
            try {
                Integer.valueOf(property)
            } catch (e: NumberFormatException) {
                LogManager.getLogger()
                    .warn("property gtfs.reader.chunksize is not a number")
                DEFAULT_CHUNK_SIZE
            }
        } else {
            DEFAULT_CHUNK_SIZE
        }

        value
    }

    init {
        gtfsHeader = parser.headerMap.keys.toList()
    }

    /**
     * Read all entities from the provided [inputStream] until reaching end of file.
     * All read entities will be dumped into the thread-local database.
     *
     * @return a list of all read entity ids
     */
    fun readRemainingEntities() {
        assert(TransactionManager.currentOrNull() != null)

        transaction {
            val buffer = ArrayList<Map<String, String>>(this@GtfsReader.chunkSize)
            while (recordIterator.hasNext()) {
                while (recordIterator.hasNext() && buffer.size < this@GtfsReader.chunkSize) {
                    val record = recordIterator.next()
                    buffer.add(gtfsHeader
                        .mapNotNull { column -> (column to record[column]?.takeIf { it.isNotEmpty() }).takeIf { it.second != null } }
                        .associate { it.first to it.second!! }
                    )
                }

                insertEntities(buffer)
                buffer.clear()
            }
        }
    }


    /**
     * Insert a new instance of a GTFS entity into the database.
     * Use the provided map to access the GTFS columns and map them to the specific SQL columns.
     *
     * @param entries a key-value store providing the column entries for the entity
     */
    protected abstract fun insertEntities(entries: Iterable<Map<String, String>>)

    override fun close() {
        this.inputStream.close()
        this.parser.close()
    }
}