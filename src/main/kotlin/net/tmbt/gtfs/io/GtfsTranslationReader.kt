package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.TableName
import net.tmbt.gtfs.model.TranslationTable
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsTranslationReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        TranslationTable.batchInsert(entries, ignore = false) { row ->
            this[TranslationTable.table] = TableName.valueOf(
                row["table_name"]?.toUpperCase()
                    ?: error("cannot create translation without table_name")
            )
            this[TranslationTable.fieldName] = row["field_name"]
                ?: error("cannot create translation without field_name")
            this[TranslationTable.language] = row["language"]
                ?: error("cannot create translation without language")
            this[TranslationTable.recordId] = row["record_id"]
            this[TranslationTable.recordSubId] = row["record_sub_id"]
            this[TranslationTable.fieldValue] = row["field_value"]
        }
    }
}