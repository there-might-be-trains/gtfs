package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.PathwayMode
import net.tmbt.gtfs.model.PathwayTable
import net.tmbt.gtfs.model.StopTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsPathwayReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        PathwayTable.batchInsert(entries, ignore = false) { row ->
            val entityId =
                EntityID(row["pathway_id"] ?: error("cannot create pathway without pathway_id"), PathwayTable)

            this[PathwayTable.id] = entityId
            this[PathwayTable.fromStopId] = EntityID(
                row["from_stop_id"]
                    ?: error("cannot create pathway without from_stop_id"), StopTable
            )
            this[PathwayTable.toStopId] = EntityID(
                row["to_stop_id"]
                    ?: error("cannot create pathway without to_stop_id"), StopTable
            )
            this[PathwayTable.pathwayMode] = PathwayMode.byOrdinalOrNull(row["pathway_mode"]?.toInt())
                ?: error("cannot create pathway without pathway_mode")
            this[PathwayTable.isBidirectional] = row["is_bidirectional"]?.let { it.toInt() == 1 }
                ?: error("cannot create pathway without is_bidirectional")
            this[PathwayTable.length] = row["length"]?.toFloat()
            this[PathwayTable.traversalTime] = row["traversal_time"]?.toInt()
            this[PathwayTable.stairCount] = row["stair_count"]?.toInt()
            this[PathwayTable.maxSlope] = row["max_slope"]?.toFloat()
            this[PathwayTable.minWidth] = row["min_width"]?.toFloat()
            this[PathwayTable.signpost] = row["signposted_as"]
            this[PathwayTable.reversedSignpost] = row["reversed_signposted_as"]
        }
    }
}