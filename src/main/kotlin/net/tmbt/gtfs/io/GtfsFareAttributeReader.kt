package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.AgencyTable
import net.tmbt.gtfs.model.FareAttributeTable
import net.tmbt.gtfs.model.PaymentMethod
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsFareAttributeReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        FareAttributeTable.batchInsert(entries, ignore = false) { row ->
            val entityId = EntityID(
                row["fare_id"] ?: error("cannot create fare attribute without id"),
                FareAttributeTable
            )

            this[FareAttributeTable.id] = entityId
            this[FareAttributeTable.price] =
                row["price"]?.toFloat() ?: error("cannot create fare attribute without price")
            this[FareAttributeTable.currency] =
                row["currency_type"] ?: error("cannot create fare attribute without currency type")
            this[FareAttributeTable.paymentMethod] =
                row["payment_method"]?.let { PaymentMethod.byOrdinalOrNull(it.toInt()) }
                    ?: error("cannot create fare attribute without payment method")
            this[FareAttributeTable.transfers] = row["transfers"]?.toInt()
            this[FareAttributeTable.agency] = row["agency_id"]?.let { EntityID(it, AgencyTable) }
            this[FareAttributeTable.transferDuration] = row["transfer_duration"]?.toInt()
        }
    }
}