package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.FrequencyTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsFrequenciesReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        FrequencyTable.batchInsert(entries, ignore = false) { row ->
            val entityId =
                EntityID(row["trip_id"] ?: error("cannot create frequency without trip id"), FrequencyTable)

            this[FrequencyTable.startTime] =
                row["start_time"] ?: error("cannot create frequency without start_time")
            this[FrequencyTable.endTime] = row["end_time"] ?: error("cannot create frequency without end_time")
            this[FrequencyTable.headway] =
                row["headway_secs"]?.toInt() ?: error("cannot create frequency without headway_secs")
            this[FrequencyTable.exact] = row["exact_times"]?.toInt() == 1
            this[FrequencyTable.id] = entityId
        }
    }
}