package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.Agency
import net.tmbt.gtfs.model.AgencyTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

/**
 * Reader for agencies.txt of the GTFS specification.
 *
 * @param inputStream an [InputStream] that contains a GTFS CSV file
 */
class GtfsAgencyReader(inputStream: InputStream) : GtfsReader(inputStream) {

    /**
     * Create an [Agency] in the current database from the values provided in the map
     */
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        AgencyTable.batchInsert(entries, ignore = false) { row ->
            val entityId = EntityID(
                row["agency_id"] ?: error("cannot create agency without id"),
                AgencyTable
            )

            this[AgencyTable.id] = entityId
            this[AgencyTable.name] = row["agency_name"] ?: error("cannot create agency without name")
            this[AgencyTable.timeZone] = row["agency_timezone"] ?: error("cannot create agency without timezone")
            this[AgencyTable.url] = row["agency_url"] ?: error("cannot create agency without url")
            this[AgencyTable.language] = row["agency_lang"]
            this[AgencyTable.phone] = row["agency_phone"]
            this[AgencyTable.fareUrl] = row["agency_fare_url"]
            this[AgencyTable.email] = row["agency_email"]
        }
    }
}