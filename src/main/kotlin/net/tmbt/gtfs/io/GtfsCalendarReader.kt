package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.CalendarTable
import net.tmbt.gtfs.model.DailyAvailability
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsCalendarReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        CalendarTable.batchInsert(entries, ignore = false) { row ->
            val entityId = EntityID(
                row["service_id"] ?: error("cannot create calendar without id"),
                CalendarTable
            )

            this[CalendarTable.id] = entityId
            this[CalendarTable.monday] = DailyAvailability.byOrdinalOrNull(row["monday"]?.toInt())
                ?: error("missing monday in calendar entry")
            this[CalendarTable.tuesday] = DailyAvailability.byOrdinalOrNull(row["tuesday"]?.toInt())
                ?: error("missing tuesday in calendar entry")
            this[CalendarTable.wednesday] = DailyAvailability.byOrdinalOrNull(row["wednesday"]?.toInt())
                ?: error("missing wednesday in calendar entry")
            this[CalendarTable.thursday] = DailyAvailability.byOrdinalOrNull(row["thursday"]?.toInt())
                ?: error("missing thursday in calendar entry")
            this[CalendarTable.friday] = DailyAvailability.byOrdinalOrNull(row["friday"]?.toInt())
                ?: error("missing friday in calendar entry")
            this[CalendarTable.saturday] = DailyAvailability.byOrdinalOrNull(row["saturday"]?.toInt())
                ?: error("missing saturday in calendar entry")
            this[CalendarTable.sunday] = DailyAvailability.byOrdinalOrNull(row["sunday"]?.toInt())
                ?: error("missing sunday in calendar entry")

            this[CalendarTable.startDate] = row["start_date"] ?: error("missing start_date in calendar entry")
            this[CalendarTable.endDate] = row["end_date"] ?: error("missing end_date in calendar entry")
        }
    }
}