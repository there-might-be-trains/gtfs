package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.*
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import java.io.InputStream

class GtfsTripReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        TripTable.batchInsert(entries, ignore = false) { row ->
            this[TripTable.id] = EntityID(row["trip_id"] ?: error("cannot create trip without trip id"), TripTable)
            this[TripTable.route] =
                EntityID(row["route_id"] ?: error("cannot create trip without route id"), RouteTable)

            // request service id in both relevant tables and then reference the matching table
            val serviceId = row["service_id"] ?: error("cannot create trip without service id")
            val calendarEntity = CalendarTable.select { CalendarTable.id eq serviceId }.firstOrNull()
            if (calendarEntity == null) {
                val calendarDateEntity =
                    CalendarDateTable.select {
                        CalendarDateTable.weakServiceId eq serviceId or (CalendarDateTable.referenceServiceId eq serviceId)
                    }.firstOrNull()
                        ?: throw IllegalStateException("cannot find Calendar or CalendarDate with id \"$serviceId\"")

                this[TripTable.serviceCalendarDate] = calendarDateEntity[CalendarDateTable.id]
            } else {
                this[TripTable.serviceCalendar] = calendarEntity[CalendarTable.id]
            }

            this[TripTable.headsign] = row["trip_headsign"]
            this[TripTable.shortName] = row["trip_short_name"]
            this[TripTable.direction] = TripDirection.byOrdinalOrNull(row["direction_id"]?.toInt())
            this[TripTable.block] = row["block_id"]
            this[TripTable.shape] = row["shape_id"]?.let { EntityID(it, ShapeTable) }
            this[TripTable.wheelchair] = Availability.byOrdinalOrNull(row["wheelchair_accessible"]?.toInt())
            this[TripTable.bikesAllowed] = Availability.byOrdinalOrNull(row["bikes_allowed"]?.toInt())
        }
    }
}