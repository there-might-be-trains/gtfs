package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.FeedInfoTable
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsFeedInfoReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        FeedInfoTable.batchInsert(entries, ignore = false) { row ->
            this[FeedInfoTable.feedPublisherName] = row["feed_publisher_name"]
                ?: error("cannot create feed_info without feed_publisher_name")
            this[FeedInfoTable.feedPublisherUrl] = row["feed_publisher_url"]
                ?: error("cannot create feed_info without feed_publisher_url")
            this[FeedInfoTable.feedLanguage] = row["feed_lang"]
                ?: error("cannot create feed_info without feed_lang")
            this[FeedInfoTable.defaultLanguage] = row["default_lang"]
            this[FeedInfoTable.feedStartDate] = row["feed_start_date"]
            this[FeedInfoTable.feedEndDate] = row["feed_end_date"]
            this[FeedInfoTable.feedVersion] = row["feed_version"]
            this[FeedInfoTable.feedContactEmail] = row["feed_contact_email"]
            this[FeedInfoTable.feedContactUrl] = row["feed_contact_url"]
        }
    }
}