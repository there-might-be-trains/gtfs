package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.Shape
import net.tmbt.gtfs.model.ShapePointTable
import net.tmbt.gtfs.model.ShapeTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.insert
import java.io.InputStream
import java.math.BigDecimal
import java.math.MathContext

class GtfsShapeReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        ShapePointTable.batchInsert(entries, ignore = false) { row ->
            val shapeEntityId =
                EntityID(row["shape_id"] ?: error("cannot create shape without shape id"), ShapeTable)

            if (Shape.find { ShapeTable.id eq shapeEntityId }.empty()) {
                ShapeTable.insert { tableRow ->
                    tableRow[ShapeTable.id] = shapeEntityId
                }
            }

            this[ShapePointTable.shapeId] = shapeEntityId
            this[ShapePointTable.latitude] = row["shape_pt_lat"]?.let { BigDecimal(it, MathContext(10)) }
                ?: error("cannot create shape point without latitude")

            this[ShapePointTable.longitude] = row["shape_pt_lon"]?.let { BigDecimal(it, MathContext(10)) }
                ?: error("cannot create shape point without longitude")

            this[ShapePointTable.sequenceNumber] = row["shape_pt_sequence"]?.toInt()
                ?: error("cannot create shape point without sequence number")

            this[ShapePointTable.distanceTraveled] = row["shape_dist_traveled"]?.toFloat()
        }
    }
}