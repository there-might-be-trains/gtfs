package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.CalendarDateTable
import net.tmbt.gtfs.model.CalendarTable
import net.tmbt.gtfs.model.ExceptionType
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.select
import java.io.InputStream

class GtfsCalendarDateReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        CalendarDateTable.batchInsert(entries, ignore = false) { row ->
            val serviceId = row["service_id"] ?: error("cannot create calendar date without service id")
            val calendarService = CalendarTable.select { CalendarTable.id eq serviceId }

            // set the service id to exactly one of reference column or varchar column
            this[CalendarDateTable.referenceServiceId] = calendarService.firstOrNull()?.get(CalendarTable.id)
            this[CalendarDateTable.weakServiceId] = if (calendarService.count() > 0) null else serviceId

            this[CalendarDateTable.date] = row["date"] ?: error("cannot create calendar date without date")
            this[CalendarDateTable.exceptionType] = ExceptionType.byOrdinalOrNull(
                row["exception_type"]?.toInt()
                    ?: error("cannot create calendar date without exception type")
            )!!
        }
    }
}