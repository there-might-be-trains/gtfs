package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.LevelTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import java.io.InputStream

class GtfsLevelReader(inputStream: InputStream) : GtfsReader(inputStream) {

    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        LevelTable.batchInsert(entries, ignore = false) { row ->
            val entityId = EntityID(
                row["level_id"] ?: error("cannot create level without id"),
                LevelTable
            )

            this[LevelTable.id] = entityId
            this[LevelTable.levelIndex] = row["level_index"]?.toFloat() ?: error("cannot crate level without index")
            this[LevelTable.levelName] = row["level_name"]
        }
    }
}