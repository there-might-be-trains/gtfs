package net.tmbt.gtfs.io

import net.tmbt.gtfs.model.Availability
import net.tmbt.gtfs.model.LevelTable
import net.tmbt.gtfs.model.LocationType
import net.tmbt.gtfs.model.StopTable
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import java.io.InputStream

class GtfsStopsReader(inputStream: InputStream) : GtfsReader(inputStream) {
    override fun insertEntities(entries: Iterable<Map<String, String>>) {
        StopTable.batchInsert(entries, ignore = false) { entries ->
            val entityId = EntityID(
                entries["stop_id"] ?: error("cannot create stop without id"),
                StopTable
            )

            this[StopTable.id] = entityId
            this[StopTable.code] = entries["stop_code"]
            this[StopTable.name] = entries["stop_name"]
            this[StopTable.desc] = entries["stop_desc"]
            this[StopTable.lat] = entries["stop_lat"]?.toBigDecimal()
            this[StopTable.lon] = entries["stop_lon"]?.toBigDecimal()
            this[StopTable.zoneId] = entries["zone_id"]
            this[StopTable.stopUrl] = entries["stop_url"]
            this[StopTable.locationType] = LocationType.byOrdinalOrNull(entries["location_type"]?.toInt())

            // reference the parent station if it exists or create a weak reference to fix later
            val parentStationId = entries["parent_station"]
            if (parentStationId != null) {
                val parentStationEntity =
                    StopTable.select { StopTable.parentStation eq parentStationId }.firstOrNull()
                if (parentStationEntity != null)
                    this[StopTable.parentStation] = parentStationEntity[StopTable.id]
                else
                    this[StopTable.weakParent] = parentStationId
            }

            this[StopTable.stopTimezone] = entries["stop_timezone"]
            this[StopTable.wheelchairBoarding] =
                Availability.byOrdinalOrNull(entries["wheelchair_boarding"]?.toInt())
            this[StopTable.levelId] = entries["level_id"]?.let { EntityID(it, LevelTable) }
            this[StopTable.platformCode] = entries["platform_code"]
        }

        // update weak keys referencing this new entity with a reference to it
        // TODO this can be made more efficient with a singular complex query after inserts. Find a way to do that
        entries.forEach { entries ->
            StopTable.update(where = { StopTable.weakParent eq entries["stop_id"] }) {
                it[parentStation] = EntityID(entries["stop_id"]!!, StopTable)
                it[weakParent] = null
            }
        }
    }
}